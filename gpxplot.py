#!/usr/bin/env python3

"""GPX Course Profile Plotter -- Plotting module
    Python Version  : 3.5.5 in windows
    Created date    : 2017-03-27
"""

import matplotlib as mpl

mpl.use('Agg')

import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties
import matplotlib.font_manager as fm
import numpy
import io

__author__ = 'Seijung Park'
__license__ = 'GPLv2'
__version__ = '0.2.11'
__date__    = '2018-04-05'


path1 = 'c:\\windows\\fonts\\nanummyeongjobold.ttf'
path2 = 'c:\\windows\\fonts\\NGULIM.ttf'
path3 = 'c:\\windows\\fonts\\h2gtrm.ttf'
path4 = 'c:\\windows\\fonts\\h2gtre.ttf'
path5 = 'C:\\Windows\\Fonts\\YSOS07.TTF'
path6 = 'C:\\Windows\\Fonts\\malgun.ttf'
path7 = 'C:\\Windows\\Fonts\\malgunbd.ttf'
path8 = 'C:\\Windows\\Fonts\\DaehanR.ttf'
path9 = 'C:\\Windows\\Fonts\\DaehanB.ttf'

path10 = '/usr/share/fonts/truetype/nanum/NanumMyeongjoBold.ttf'
path11 = '/usr/share/fonts/truetype/nanum/NanumGothic.ttf'

fontprop1 = fm.FontProperties(fname=path10, size=8)
fontprop2 = fm.FontProperties(fname=path11, size=12)
fontprop3 = fm.FontProperties(fname=path11, size=10)
fontprop4 = fm.FontProperties(fname=path11, size=8)

MAX_GRAD = 20
#colormap  = mpl.cm.jet
#colormap  = mpl.cm.rainbow
#norm      = mpl.colors.Normalize(vmin=-MAX_GRAD, vmax=MAX_GRAD)


def no_func(t):
    print('버전: ', mpl.__version__)
    print('설치 위치: ', mpl.__file__)
    print('설정 위치: ', mpl.get_configdir())
    print('캐시 위치: ', mpl.get_cachedir())
    print('설정파일 위치: ', mpl.matplotlib_fname())
    font_list = fm.findSystemFonts(fontpaths=None, fontext='ttf')
    print(len(font_list))
    print(font_list[:10])
    # print([(f.name, f.fname) for f in fm.fontManager.ttflist if 'Nanum' in f.name])
    print([(f.name, f.fname) for f in fm.fontManager.ttflist if 'Yj' in f.name])


def plot_cp(xlist, ylist, length, height, xdelta, dy1, dy2, w, rotate):
    bboxval = dict(facecolor='#ffffff', alpha=0.4,edgecolor='#ffffff',boxstyle='round')
    alignment2 = {'horizontalalignment': 'left', 'verticalalignment': 'top'}
    alignment3 = {'horizontalalignment': 'right', 'verticalalignment': 'top'}

    plt.plot((xlist[w.pos],xlist[w.pos]), (0,height), linewidth=1.6, color='red')
    km = '{:.0f} / {:.0f} km'.format(w.dkm, w.km)
    ascen = '{:.0f} / {:.0f} m'.format(w.dascen, w.ascen)
    # flip right & down if position is near 12/100
    #if (xlist[w.pos] < length *.12) or (w.name[-2] == '_' and w.name[-1] == 'R'):
    dyy = 0
    align = alignment3
    if (xlist[w.pos] < length *.12 and w.name[-3] != '_'):
        w.name = w.name + '_R2'

    if len(w.name) > 3 and w.name[-3] == '_':
        dyy = height * int(w.name[-1]) * 0.07
        
        if w.name[-2] == 'R':
            align = alignment2
            xdelta = -xdelta
        else:
            align = alignment3
            
        w.name = w.name[:-3]
        
    plt.text(xlist[w.pos]-xdelta, height*.92-dyy, w.name,fontproperties=fontprop3, color='darkblue', **align, bbox=bboxval, rotation=rotate)
    plt.text(xlist[w.pos]-xdelta, dy1-dyy, km,fontproperties=fontprop4, color='r', **align, bbox=bboxval, rotation=rotate)
    plt.text(xlist[w.pos]-xdelta, dy2-dyy, ascen,fontproperties=fontprop4, color='r', **align, bbox=bboxval, rotation=rotate)


def plot_text(xlist, ylist, xdelta, ydelta, w):
    name = w.name[1:]
    print(name)
    if len(name) >3 and name[-3] == '_':
        if name[-2] == 'u':
            valign = 'bottom'
            ydelta = ydelta * 1
        elif name[-2] == 'U':
            valign = 'bottom'
            ydelta = ydelta * 2
        elif name[-2] == 'l':
            valign = 'top';
            ydelta = ydelta * -1
        elif name[-2] == 'L':
            valign = 'top';
            ydelta = ydelta * -2
        elif name[-2] == 'C' or name[-2] == 'c':
            valign = 'center'
            ydelta = 0
        else:
            valign = 'center'
            ydelta = 0
        if name[-1] == '1':
            halign = 'right'
            xdelta = xdelta * -1.2
        elif name[-1] == '4':
            halign = 'right'
            xdelta = xdelta * -2.4
        elif name[-1] == '3':
            halign = 'left'
            xdelta = xdelta * 1.2
        elif name[-1] == '6':
            halign = 'left'
            xdelta = xdelta * 2.4
        elif name[-1] == '2' or name[-1] == '5':
            halign = 'center'
            xdelta = 0
        else:
            halign = 'left'
            xdelta = xdelta * 1.2
        name = name[:-3]
    else:
        valign = 'bottom'
        ydelta = ydelta * -1
        halign = 'left'
    bboxval = dict(facecolor='#ffffff', alpha=0.4,edgecolor='#ffffff',boxstyle='round')
    plt.text(xlist[w.pos]+xdelta, ylist[w.pos]+ydelta, name,fontproperties=fontprop4, color='k', ha=halign,va=valign,bbox=bboxval)


def grad2color( xlist, ylist, i):
    dx = xlist[i+1] - xlist[i]
    dy = ylist[i+1] - ylist[i]
    if (dx == 0):
        dx = 0.5        # to prevent zero devide
    grad = dy / (dx*10)
    if (grad > MAX_GRAD):
        grad = MAX_GRAD
    if (grad < -MAX_GRAD):
        grad = -MAX_GRAD
   
    #color = colormap( norm(grad) )
    #print( dx, dy, grad, norm(grad))

    if (grad > 11):
            return '#ff000d'
    elif (grad > 6): 
            return '#b8006a'
    elif (grad > 3): 
            return '#aa00aa'
    elif (grad > 1.5): 
            return '#6600cc'
    elif (grad < -10):
        return '#003333'
    elif (grad < -5):
        return '#1166cc'
    return '#1111ff'
    
    """
    if (grad <= 0):
        return mpl.cm.gnuplot2( -grad / MAX_GRAD * .5 )
    else:
        #return mpl.cm.hot( grad / MAX_GRAD *.5 )
        return mpl.cm.gist_heat( grad / MAX_GRAD *.8 )
    """
    

def do_plot(xlist, ylist, w_list, s_list, name, rotate=0, colorize=None):
    font0 = FontProperties()
    alignment1 = {'horizontalalignment': 'center', 'verticalalignment': 'baseline'}
    alignment2 = {'horizontalalignment': 'left', 'verticalalignment': 'top'}
    alignment3 = {'horizontalalignment': 'right', 'verticalalignment': 'top'}
    # Show family options
    families = ['serif', 'sans-serif', 'cursive', 'fantasy', 'monospace']
    font1 = font0.copy()
    font1.set_size('large')
    bboxval = dict(facecolor='#ffffff', alpha=0.4,edgecolor='#ffffff',boxstyle='round')

    length = max(xlist)
    height = max(ylist)
    height = int((height + 99.9) / 100) * 100
    height = max(height, 300)
    if (height == 400):
        dy1 = height * .827
        dy2 = height * .733
    elif height == 300:
        dy1 = height * .815
        dy2 = height * .736
    elif height == 600:
        dy1 = height * .821
        dy2 = height * .733
    else:
        dy1 = height *.82
        dy2 = height *.73
        
    xdelta = length / 150
    ydelta = height / 20

    figure = plt.figure(num=None, figsize=(15, 2.9), dpi=240, facecolor='w', edgecolor='k')
    # plt.axis([0, length, 0, height])
    ax = figure.gca()
    #ax.set_yscale('log')
    ax.set_xticks(numpy.arange(0, length, 20))
    if height >= 550:
        ax.set_yticks(numpy.arange(0, height, 100))
    else:
        ax.set_yticks(numpy.arange(0, height, 50))
        
    plt.grid(True, linewidth=0.7)
    plt.xlim(0,length)
    plt.ylim(0,height)
    #plt.ylim(0,200)

    #plt.plot([0,20,40,60,65,80,90,110,200,150], linewidth=1.5)
    
    for w in w_list:
        plot_cp(xlist, ylist, length, height, xdelta, dy1, dy2, w, rotate)
    
    
    for i in range(len(xlist)-1):
        if colorize is None:
            plt.plot( (xlist[i], xlist[i+1]), (ylist[i], ylist[i+1]), linewidth=1.2, color='b')
        else:
            color = grad2color( xlist, ylist, i)
            plt.plot( (xlist[i], xlist[i+1]), (ylist[i], ylist[i+1]), linewidth=1.6, color=color)
    
    
    for w in s_list:
        #plt.text(xlist[w.pos]+xdelta/2, ylist[w.pos]-24, w.name[1:],fontproperties=fontprop2, color='k', fontsize=11, **alignment2)
        plt.plot(xlist[w.pos], ylist[w.pos], '^',color='#ffff00', ms=7,mew=1.2,mec='#330066')
        plot_text(xlist, ylist, xdelta, ydelta, w)

    #plt.ylabel('Altitude')
    #plt.xlabel('거리[km]', fontproperties=fontprop1)
    #plt.title(name[0:-4])

    #bboxval = dict(facecolor='#ffff66', alpha=0.8,edgecolor='#ffff66',boxstyle='round')
    bboxval = dict(facecolor='#99ffff', alpha=0.8,edgecolor='#ddffff',boxstyle='round')
    plt.text(length*.01, height*.955, name[0:-4], fontproperties=fontprop2, **alignment2, color='darkgreen', bbox=bboxval)
    
    buf = io.BytesIO()
    plt.savefig(buf, dpi=200, bbox_inches='tight', format='png')
    plt.close()
    buf.seek(0)
    data = buf.read()
    buf.close()


    return (length, height, data)
    #no_func(1)


def main():
    x = numpy.linspace(-15,15,500)
    y = numpy.sin(x)/x
    x = x * 20 + 200
    y = y * 100 +200
    do_plot(x, y,'none')


if __name__ == '__main__':
	main()
