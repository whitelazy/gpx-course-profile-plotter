import os
from flask import Flask, request, redirect, url_for, send_file
from werkzeug import secure_filename
from flask import send_from_directory
import gpxcpp
import io
import sys
from multiprocessing import Process, Queue

UPLOAD_FOLDER = './files'
ALLOWED_EXTENSIONS = {'gpx'}

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


@app.route('/', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        file = request.files['file']
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            # file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            return redirect(url_for('uploaded_file',
                                    filename=filename), code=307)
    return '''
    <!doctype html>
    <title>Upload gpx File</title>
    <h1>Upload gpx File</h1>
    <form action="" method=post enctype=multipart/form-data>
      <p><input type=file name=file>
         <input type=submit value=Upload></p>
      <p><input type=checkbox name=rotate value=30> text rotation 30 degree</p>
    </form>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <p>Made by <a href='https://bitbucket.org/psijoy/gpx-course-profile-plotter'>간큰남자</a><br>
Packaged by <a href='https://bitbucket.org/whitelazy/gpx-course-profile-plotter'>whitelazy</a><br>
Squeezed by muderer </p>
    '''
# <p>text rotation : <input type="number" min="0" max="180" value="0" name=rotate></p>


def run_process(queue, data, filename, rotate, colorize):
    queue.put(gpxcpp.do_plot_gpx_to_png(data, filename, rotate=rotate, colorize=colorize))


@app.route('/uploads/<filename>', methods=['GET', 'POST'])
def uploaded_file(filename):
    if request.method == 'POST':
        file = request.files['file']
        rotation = 0
        if 'rotate' in request.form:
            rotation = request.form['rotate']
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            q = Queue()
            p = Process(target=run_process, args=(q, file.stream._file.read().decode('UTF-8'), filename, rotation, 'y'))
            p.start()

            (length, height, image) = q.get()
            p.join()

            print("Length: {}, Height: {}".format(length, height))
            return send_file(io.BytesIO(image), attachment_filename=filename, mimetype='image/png')
    return ''

port = 5000
if len(sys.argv) >= 2:
    port = int(sys.argv[1])
app.run(host='0.0.0.0', port=port, threaded=True)
