#!/usr/bin/env python3
# # -*- coding: utf-8 -*-

"""GPX Course Profile Plotter -- main
    Python Version  : 3.5.5 in windows
    Created date    : 2017-03-27
"""
import sys

from . import gpxplot
import gpxpy.parser as parser
from geopy.distance import vincenty
from geopy.distance import great_circle
import ntpath

__author__ = 'Seijung Park'
__license__ = 'GPLv2'
__version__ = '0.2.11'
__date__    = '2018-04-05'



def get_nearest( points, wpt):
    near = []
    for i in range(len(points)):
        dist = vincenty( (points[i].latitude, points[i].longitude), (wpt.latitude, wpt.longitude)).meters
        if (dist < 20):
            print( "Found pos:%4d, dist:%3dm" %  (i, dist))
            near.append( points[i] )
            print( points[i] )
            return i
        #print( p, dist)
    return 0


class my_wpt:
    def __init__(self):
        self.name = []
        self.pos = 0    # x, y array position
        self.km = 0
        self.dkm = 0
        self.ascen = 0
        self.dascen = 0

    def __str__(self):
        return '[my_wpt{%s}:%s,%s %s: %s,%s]' % (self.name, self.pos, self.km, self.dkm, self.ascen, self.dascen)

    def minimum_pt(self, x, y, i, j):
        if x < y:
            return x, i
        else:
            return y, j

    def get_nearest(self, points, wpt):
        self.name = wpt.name
        near = []
        minimum_dist, index = sys.maxsize, 0

        for i in range(len(points)):
            dist = vincenty((points[i].latitude, points[i].longitude), (wpt.latitude, wpt.longitude)).meters
            minimum_dist, index = self.minimum_pt(minimum_dist, dist, index, i)

        print(index)
        self.name = wpt.name
        self.pos = index
        return index

    def set_last(self, points):
        self.name = '완주'
        self.pos = len(points) - 1
        return
            
    def get_km( self, prev, x):
        self.km = x[self.pos]
        if prev is None:
            self.dkm = self.km
        else:
            self.dkm = self.km - prev.km
        return

    def get_ascen( self,  prev, y):
        self.ascen = y[self.pos]
        if prev is None:
            self.dascen = self.ascen
        else:
            self.dascen = self.ascen - prev.ascen
        return
    


def calc_dist( points):
    x = []
    y = []
    ascen = []

    lenth = 0
    upelev = 0
    print( "\n" )
    lenth = len( points )
    point0 = points[0]
    point1 = points[1]

    sum1 = 0
    sum2 = 0
    x.append(0)
    y.append(0)
    ascen.append(0)
    
    for i in range(1, lenth):
        lat0 = points[i-1].latitude
        lon0 = points[i-1].longitude
        lat1 = points[i].latitude
        lon1 = points[i].longitude
    
        dist1 = vincenty( (lat0, lon0), (lat1, lon1)).meters
        sum1 += dist1
        if dist1 == 0:
            dist1 = 0.5     # defend zero devide
        sum2 = sum1 / 1000

        # elevation값이 None인 경우 이전 포인트값 쓰도록 수정
        if points[i].elevation is None:
            points[i].elevation = points[i-1].elevation
        elev = points[i].elevation
        print("index (%d)" % i)
        delev = (elev - points[i - 1].elevation)

        # factor(4.0) is calibrated. do not modify
        # ignore single up point
        #el0 = 
        #el1
        #el2
        if delev > 4.0:
            upelev += delev
        grad = delev / dist1 * 100
        #print( "%7.1f, %8.3f %5.1f %4.1f" % (dist1, sum2, elev, grad))
        x.append(sum1/1000)
        y.append(elev)
        ascen.append(upelev)
    print( "Total up elevation: %f\n" % (upelev))
    return x, y, ascen


def do_plot_gpx_to_png(gpx_xml, f_name, rotate=0, colorize=None):
    wpt_list = []
    s_list = []

    gpx_parser = parser.GPXParser(gpx_xml)
    gpx_parser.parse()

    gpx = gpx_parser.parse()

    x, y, ascen = calc_dist(gpx.tracks[0].segments[0].points)

    for waypoint in gpx.waypoints:
        #print ('waypoint {0} -> ({1},{2})'.format( waypoint.name, waypoint.latitude, waypoint.longitude ))
        print( waypoint )
        #pos = get_nearest( gpx.tracks[0].segments[0].points, waypoint)
        w = my_wpt()
        pos = w.get_nearest( gpx.tracks[0].segments[0].points, waypoint)
        
        if w.name[0] == 's':
            s_list.append(w)
        else:
            wpt_list.append(w)

    wpt_list.sort(key=lambda my_wpt : my_wpt.pos)
        
     
    for i in range(len(wpt_list)):
        if i == 0:
            wpt_list[i].get_km( None, x)
            wpt_list[i].get_ascen( None, ascen)
        else:
            wpt_list[i].get_km( wpt_list[i-1], x)
            wpt_list[i].get_ascen( wpt_list[i-1], ascen)

            #wpt.append(pos)
            #wpt_name.append(waypoint.name)

    if (len(x) - wpt_list[-1].pos) > 5:
        w = my_wpt()
        w.set_last( gpx.tracks[0].segments[0].points )
        
        if len(wpt_list) == 0:
            w.get_km( None, x)
            w.get_ascen( None, ascen)
        else:
            print( len(x), len(ascen))
            w.get_km( wpt_list[-1], x)
            w.get_ascen( wpt_list[-1], ascen)
        wpt_list.append( w )
    
    for w in wpt_list:
        print(w)

    name = ntpath.basename(f_name)
    return gpxplot.do_plot(x, y, wpt_list, s_list, name, rotate, colorize)


def do_job(f_name):
    with open(f_name, 'r', encoding='UTF8') as f:
        (length, height, image) = do_plot_gpx_to_png(f.read(), f_name)
    print("Length: {}, Height: {}".format(length, height))

    with open(f_name[0:-4]+'.png', 'wb') as i:
        i.write(image)


def main():
    do_job('./S-400K.gpx')
    do_job('./B-400K.gpx')
    do_job('./Seoul to Busan.gpx')
    #do_job('D:/psj/개인자료/자전거/경로/50란도너스/퍼머넌트/PT-19_20_21_25  백두대간/PT-19-20170618-inv.gpx')
    #do_job( '../GPX/sample0.gpx')
	
	
if __name__ == '__main__':
	main()
