FROM python:3.6.4-stretch

RUN apt-get update && apt-get install git \
    && mkdir -p /var/flask/gpx-course-profile-plotter
RUN apt-get install -y fonts-nanum

RUN set -x && git clone https://whitelazy@bitbucket.org/whitelazy/gpx-course-profile-plotter.git /var/flask/gpx-course-profile-plotter \
    && cd /var/flask/gpx-course-profile-plotter \
    && pip install -r requirements.txt

COPY entrypoint.sh /entrypoint.sh
RUN chmod u+x /entrypoint.sh
EXPOSE 5000
WORKDIR /var/flask/gpx-course-profile-plotter

ENTRYPOINT ["/entrypoint.sh"]
